// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { MatSnackBarConfig } from '@angular/material/snack-bar';

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCHRCJ1IGj7RSkyYlqHffucZIb0hibvgN0',
    authDomain: 'fbi-wanted-88ba0.firebaseapp.com',
    projectId: 'fbi-wanted-88ba0',
    storageBucket: 'fbi-wanted-88ba0.appspot.com',
    messagingSenderId: '583747691466',
    appId: '1:583747691466:web:cbc79a1057348772d22533',
    measurementId: 'G-MX3BMST3G9'
  }
};

export const snackBarConfig: MatSnackBarConfig<object> = {
  duration: 3000,
  horizontalPosition: 'center',
  verticalPosition: 'top'
};

export const dialogOpenConfig = {
  height: '400px',
  width: '400px'
};

export const urlApi = 'http://localhost:4200/api';
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
