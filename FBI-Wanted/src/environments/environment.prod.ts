import { MatSnackBarConfig } from '@angular/material/snack-bar';

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCHRCJ1IGj7RSkyYlqHffucZIb0hibvgN0',
    authDomain: 'fbi-wanted-88ba0.firebaseapp.com',
    projectId: 'fbi-wanted-88ba0',
    storageBucket: 'fbi-wanted-88ba0.appspot.com',
    messagingSenderId: '583747691466',
    appId: '1:583747691466:web:cbc79a1057348772d22533',
    measurementId: 'G-MX3BMST3G9'
  }
};

export const snackBarConfig: MatSnackBarConfig<object> = {
  duration: 3000,
  horizontalPosition: 'center',
  verticalPosition: 'top'
};

export const dialogOpenConfig = {
  height: '400px',
  width: '400px'
};

export const urlApi = 'http://localhost:4200/api';
