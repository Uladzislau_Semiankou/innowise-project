import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponentModule } from './components/settings/settings-component.module';
import { SettingsRoutingModule } from './settings-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SettingsComponentModule,
    SettingsRoutingModule,
  ],
  exports: []
})
export class SettingsModule { }
