export interface Wanted {
  uid: string
  sex: string;
  nationality: string;
  place_of_birth: string;
  images: Img[];
  details: string;
  caution: string;
  remarks: string;
  field_offices: string;
  description: string;
  dates_of_birth_used: string[];
  title: string;
  person_classification: string;
}

interface Img {
  caption: string;
  large: string;
  original: string;
  thumb: string;
}
