export interface WantedContent {
  sex: string;
  title: string;
  person_classification: string;
}
