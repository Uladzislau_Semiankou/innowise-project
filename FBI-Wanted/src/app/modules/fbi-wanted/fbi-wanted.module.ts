import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FbiWantedContentModule } from './components/fbi-wanted-content/fbi-wanted-content.module';
import { FbiWantedRoutingModule } from './fbi-wanted-routing.module';
import { FbiWantedDetailsModule } from './components/fbi-wanted-details/fbi-wanted-details.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FbiWantedDetailsModule,
    FbiWantedContentModule,
    FbiWantedRoutingModule
  ],
  exports: []
})
export class FbiWantedModule { }
