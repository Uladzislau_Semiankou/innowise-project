import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FBIWantedContentComponent } from './components/fbi-wanted-content/fbi-wanted-content.component';
import { FbiWantedDetailsComponent } from './components/fbi-wanted-details/fbi-wanted-details.component';


const routes: Routes = [
  {
    path: '',
    component: FBIWantedContentComponent,
    children: [
      {
        path: 'details/:page/:id',
        component: FbiWantedDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FbiWantedRoutingModule {
}
