import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FbiWantedListComponent } from './fbi-wanted-list.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DefaultValueModule } from '../../../../shared/pipes/default-value/default-value.module';


@NgModule({
  declarations: [FbiWantedListComponent],
  imports: [
    CommonModule,
    MatListModule,
    RouterModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    DefaultValueModule
  ],
  exports: [FbiWantedListComponent]
})
export class FbiWantedListModule {
}
