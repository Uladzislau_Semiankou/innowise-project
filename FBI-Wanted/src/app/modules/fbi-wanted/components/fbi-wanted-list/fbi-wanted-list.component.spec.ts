import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedListComponent } from './fbi-wanted-list.component';

describe('FbiWantedListComponent', () => {
  let component: FbiWantedListComponent;
  let fixture: ComponentFixture<FbiWantedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
