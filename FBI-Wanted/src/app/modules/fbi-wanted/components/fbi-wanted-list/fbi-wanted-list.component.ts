import { ChangeDetectorRef, Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FbiWantedService } from '../../services/fbi-wanted.service';
import { Wanted } from '../../interfaces/wanted.interface';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PageEvent } from '@angular/material/paginator';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-list',
  templateUrl: './fbi-wanted-list.component.html',
  styleUrls: ['./fbi-wanted-list.component.scss']
})
export class FbiWantedListComponent implements OnInit {
  @Input() public wantedList: Wanted[];
  @Input() public wantedTotal: number;
  @Output() selectOption: EventEmitter<Wanted> = new EventEmitter<Wanted>();
  @Output() selectPage: EventEmitter<number> = new EventEmitter<number>();

  public page = 1;
  public select = false;
  public viewRow = ['title', 'sex', 'classification'];

  constructor(private fbiWanted: FbiWantedService, private changeDetector: ChangeDetectorRef) {
  }

  public ngOnInit(): void {
    this.fbiWanted.getStartWanted()
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
        this.wantedList = data.items;
        this.wantedTotal = Number(data.total);
        this.changeDetector.markForCheck();
      });
  }

  public onSelect(wanted: Wanted): void {
    this.selectOption.emit(wanted);
    this.select = true;
  }

  public onSelectPage(): void {
    this.selectPage.emit(this.page);
  }

  public onPaginationChange(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    this.onSelectPage();
    this.fbiWanted.getWantedByPage(String(this.page))
      .pipe(untilDestroyed(this))
      .subscribe(((data) => {
        this.wantedList = data;
        this.changeDetector.markForCheck();
      }));
  }

}
