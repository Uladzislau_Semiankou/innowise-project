import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Wanted } from '../../interfaces/wanted.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FbiWantedService } from '../../services/fbi-wanted.service';
import { UserResponse } from '../../interfaces/user-response.interface';
import { MatAutocomplete } from '@angular/material/autocomplete';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-content',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted-content.component.html',
  styleUrls: ['./fbi-wanted-content.component.scss']
})
export class FBIWantedContentComponent implements OnInit, OnDestroy {
  public wantedList: UserResponse;
  public isDetailsShow: boolean;
  public selectOptionByUid: string;
  public selectPage: number;
  public filterForm: FormGroup;
  public fieldsArray = ['title', 'sex', 'person_classification'];
  public valuesArray = new Set;

  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private formBuild: FormBuilder,
    public fbiWanted: FbiWantedService
  ) {
  }

  public ngOnInit(): void {
    this.filterForm = this.formBuild.group({
      fieldToFilter: ['', [Validators.required]],
      valueToFilter: ['', [Validators.required]]
    });

    this.fbiWanted.getStartWanted().pipe(untilDestroyed(this)).subscribe((data) => {
      this.wantedList = data;

      console.log(this.valuesArray);
      this.changeDetector.markForCheck();
    });

    this.isDetailsShow = this.router.url !== '/fbi-wanted';
  }

  public ngOnDestroy(): void {
  }

  public openDetails(): void {
    if (this.isDetailsShow) {
      this.router.navigate(['fbi-wanted']);
    } else {
      this.router.navigate(['fbi-wanted/details', this.selectPage.toString(), this.selectOptionByUid]);
    }
    this.isDetailsShow = !this.isDetailsShow;
    this.changeDetector.markForCheck();
  }

  public transferUID(wanted: Wanted): void {
    this.selectOptionByUid = wanted.uid;
  }

  public transferPage(page: number): void {
    this.selectPage = page;
    this.sendFilterRequest(page);
  }

  public sendFilterRequest(page = 1): void {
    this.fbiWanted.getWantedByField(this.filterForm.value.fieldToFilter, this.filterForm.value.valueToFilter, String(page))
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
        this.wantedList = data;
        this.changeDetector.markForCheck();
      });
  }

  public refreshFilterRequest(): void {
    this.resetForm();
    this.sendFilterRequest();
  }

  public resetForm(): void {
    this.filterForm.reset();
    this.filterForm.markAsUntouched();
  }

  public select(): void {
    this.valuesArray.clear();
    this.wantedList.items.map((item) => {
      if (Boolean(item[this.filterForm.value.fieldToFilter])) {
        this.valuesArray.add(String(item[this.filterForm.value.fieldToFilter]).toLowerCase());
      }
    });
  }


}
