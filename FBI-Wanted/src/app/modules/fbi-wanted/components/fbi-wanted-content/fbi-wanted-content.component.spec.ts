import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FBIWantedContentComponent } from './fbi-wanted-content.component';

describe('FBIWantedContentComponent', () => {
  let component: FBIWantedContentComponent;
  let fixture: ComponentFixture<FBIWantedContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FBIWantedContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FBIWantedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
