import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FBIWantedContentComponent } from './fbi-wanted-content.component';
import { RouterModule } from '@angular/router';
import { FbiWantedListModule } from '../fbi-wanted-list/fbi-wanted-list.module';
import { FbiWantedDetailsModule } from '../fbi-wanted-details/fbi-wanted-details.module';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';


@NgModule({
  declarations: [FBIWantedContentComponent],
  imports: [
    CommonModule,
    RouterModule,
    FbiWantedListModule,
    FbiWantedDetailsModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule
  ],
  exports: [FBIWantedContentComponent]
})
export class FbiWantedContentModule {
}
