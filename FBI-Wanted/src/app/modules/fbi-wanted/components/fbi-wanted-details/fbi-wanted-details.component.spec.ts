import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedDetailsComponent } from './fbi-wanted-details.component';

describe('FbiWantedDetailsComponent', () => {
  let component: FbiWantedDetailsComponent;
  let fixture: ComponentFixture<FbiWantedDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
