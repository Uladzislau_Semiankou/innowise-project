import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FbiWantedService } from '../../services/fbi-wanted.service';
import { Wanted } from '../../interfaces/wanted.interface';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-details',
  templateUrl: './fbi-wanted-details.component.html',
  styleUrls: ['./fbi-wanted-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FbiWantedDetailsComponent implements OnInit, OnDestroy {
  public id: string;
  public page: number;
  public wanted: Wanted;

  constructor(
    private activateRoute: ActivatedRoute,
    private fbiWanted: FbiWantedService,
    private changeDetector: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.getParams();
    this.getWanted();
  }

  public ngOnDestroy(): void {
  }

  public getParams(): void {
    this.activateRoute.params
      .pipe(untilDestroyed(this))
      .subscribe(params => {
        this.page = params.page;
        this.id = params.id;
      });
  }

  public getWanted(): void {
    this.fbiWanted.getWantedByUid(String(this.page), this.id)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        [this.wanted] = data;
        this.changeDetector.markForCheck();
      });
  }
}
