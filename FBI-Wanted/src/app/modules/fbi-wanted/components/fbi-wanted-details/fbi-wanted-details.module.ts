import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FbiWantedDetailsComponent } from './fbi-wanted-details.component';
import { DefaultValueModule } from '../../../../shared/pipes/default-value/default-value.module';
import { PurifyModule } from '../../../../shared/components/purify/purify.module';


@NgModule({
  declarations: [FbiWantedDetailsComponent],
  imports: [
    CommonModule,
    DefaultValueModule,
    PurifyModule
  ],
  exports: [FbiWantedDetailsComponent]
})
export class FbiWantedDetailsModule { }
