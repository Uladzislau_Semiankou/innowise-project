import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UserResponse } from '../interfaces/user-response.interface';
import { Observable } from 'rxjs';
import { Wanted } from '../interfaces/wanted.interface';
import { urlApi } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FbiWantedService {
  constructor(private http: HttpClient) {
  }

  public getStartWanted(): Observable<any> {
    return this.http.get<UserResponse>(`${urlApi}?page=1`);
  }

  public getWantedByPage(page: string): Observable<any> {
    return this.http.get<UserResponse>(urlApi, {
      params: new HttpParams().set('page', page)
    }).pipe(
      map(response => response.items)
    );
  }

  public getWantedByUid(page: string, uid: string): Observable<any> {
    return this.http.get<UserResponse>(urlApi, {
      params: new HttpParams().set('page', page)
    }).pipe(
      map(response => {
        return response.items.filter((elem: Wanted) => elem.uid === uid);
      })
    );
  }

  public getWantedByField(field: string, value: string, page: string): Observable<UserResponse> {
    return this.http.get<UserResponse>('http://localhost:4200/api', {
      params: new HttpParams().set(field, value).set('page', page)
    });
  }
}
