import { TestBed } from '@angular/core/testing';

import { FbiWantedService } from './fbi-wanted.service';

describe('FbiWantedService', () => {
  let service: FbiWantedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FbiWantedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
