import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../../../shared/services/auth.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Router } from '@angular/router';
import { SnackBarService } from '../../../../shared/services/snack-bar.service';
import { DialogComponent } from '../../../../shared/components/dialog/dialog.component';
import { dialogOpenConfig } from '../../../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';

@UntilDestroy()
@Component({
  selector: 'fw-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  constructor(
    public authService: AuthService,
    private router: Router,
    private snackBarService: SnackBarService,
    private dialog: MatDialog
  ) {
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }

  public logout(): void {
    this.authService.logout()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.router.navigate(['home']);
        this.snackBarService.openSnackBar();
      });
  }

  public openDialog(): void {
    this.dialog.open(DialogComponent, dialogOpenConfig);
  }
}
