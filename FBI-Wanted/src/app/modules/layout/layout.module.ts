import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './components/header/header.module';
import { MainModule } from './components/main/main.module';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HeaderModule,
    MainModule
  ],
  exports: [HeaderModule, MainModule]
})
export class LayoutModule { }
