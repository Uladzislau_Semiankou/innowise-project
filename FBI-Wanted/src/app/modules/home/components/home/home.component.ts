import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../../shared/components/dialog/dialog.component';
import { dialogOpenConfig } from '../../../../../environments/environment';

@Component({
  selector: 'fw-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  public email = '';
  public password = '';

  constructor(private dialog: MatDialog) {
  }

  public ngOnInit(): void {
  }

  public openDialog(): void {
    this.dialog.open(DialogComponent, dialogOpenConfig);
  }
}
