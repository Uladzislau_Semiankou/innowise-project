import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponentModule } from './components/home/home-component.module';
import { HomeRoutingModule } from './home-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HomeComponentModule,
    HomeRoutingModule
  ],
  exports: []
})
export class HomeModule { }
