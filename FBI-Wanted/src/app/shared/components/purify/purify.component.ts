import { Component, OnInit, ChangeDetectionStrategy, Input, SecurityContext } from '@angular/core';
import { NgDompurifySanitizer } from '@tinkoff/ng-dompurify';

@Component({
  selector: 'fw-purify',
  templateUrl: './purify.component.html',
  styleUrls: ['./purify.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PurifyComponent implements OnInit {
  @Input() htmlFragment: string;

  constructor(private readonly dompurifySanitizer: NgDompurifySanitizer) {
  }

  public ngOnInit(): void {
  }

  public purify(value: string | null): string | undefined {
    if (!value) {
      return undefined;
    }
    return this.dompurifySanitizer.sanitize(SecurityContext.HTML, value);
  }

}
