import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurifyComponent } from './purify.component';



@NgModule({
  declarations: [PurifyComponent],
  imports: [
    CommonModule
  ],
  exports: [PurifyComponent]
})
export class PurifyModule { }
