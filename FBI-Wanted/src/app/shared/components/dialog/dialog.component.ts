import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'fw-dialog',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  public loginForm: FormGroup;
  public hide = true;
  public isUserInDatabase: boolean;

  constructor(private formBuild: FormBuilder, private authService: AuthService) {
  }

  public ngOnInit(): void {
    this.loginForm = this.formBuild.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  public isControlInvalid(controlName: string): boolean {
    const control = this.loginForm.controls[controlName];
    return control.invalid && control.touched;
  }

  public isControlFilled(controlName: string): boolean {
    const control = this.loginForm.controls[controlName];
    return control.hasError('required');
  }

  public resetForm(): void {
    this.loginForm.reset();
    this.loginForm.markAsUntouched();
  }

  public logIn(): void {
    this.authService.logIn(this.loginForm.value.email, this.loginForm.value.password);
    this.resetForm();
  }

  public togglePassword(): void {
    this.hide = !this.hide;
  }
}
