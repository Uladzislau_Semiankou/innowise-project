import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import firebase from 'firebase';
import firestore = firebase.firestore;

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private firestoreService: AngularFirestore) {
  }

  public createUser(collectionName: string, data: {}): Observable<string> {
    return from(this.firestoreService.collection(collectionName).add(data)).pipe(
      map((value) => value.id)
    );
  }

  public checkUser(collectionName: string, email: string): Observable<DocumentChangeAction<unknown>[]> {
    return this.firestoreService
      .collection(collectionName, (ref) => {
        const query: firestore.Query = ref;
        return query.where('email', '==', email);
      })
      .snapshotChanges()
      .pipe(
        take(1)
      );
  }
}
