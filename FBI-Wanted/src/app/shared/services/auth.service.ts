import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';
import auth = firebase.auth;
import { from, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { CrudService } from './crud.service';
import UserCredential = firebase.auth.UserCredential;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user$: Observable<firebase.User | null>;

  constructor(private angAuthService: AngularFireAuth, private crudService: CrudService) {
    this.user$ = angAuthService.authState;
  }


  public createUser(email: string, password: string): Observable<UserCredential> {
    return from(this.angAuthService
      .createUserWithEmailAndPassword(email, password)).pipe(
      tap((userCred: auth.UserCredential) => {
        this.crudService.createUser('users', {
          role: 'user',
          email: userCred.user?.email
        });
      }),
      take(1)
    );
  }

  public signIn(email: string, password: string): Observable<UserCredential> {
    return from(this.angAuthService
      .signInWithEmailAndPassword(email, password)).pipe(
      tap((userCred: auth.UserCredential) => {
        console.log(userCred);
      }),
      take(1)
    );
  }

  public logIn(email: string, password: string): void {
    this.crudService.checkUser('users', email).subscribe((data) => {
      if (Boolean(data.length)) {
        this.signIn(email, password);
        return;
      }
      this.createUser(email, password);
    });
  }

  public logout(): Observable<void> {
    return from(this.angAuthService
      .signOut());
  }
}
