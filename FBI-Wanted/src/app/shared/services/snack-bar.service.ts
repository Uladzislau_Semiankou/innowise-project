import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { snackBarConfig } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {
  }

  public openSnackBar(): void {
    this.snackBar.open('Please log in to use the application', 'Close', snackBarConfig);
  }
}
