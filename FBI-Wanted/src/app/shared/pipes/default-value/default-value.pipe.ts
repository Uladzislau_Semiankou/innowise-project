import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultValue'
})
export class DefaultValuePipe implements PipeTransform {

  transform(value: string | null, defaultData = 'No data'): string {
    if(!value) {
      return defaultData;
    }
    return value;
  }

}
