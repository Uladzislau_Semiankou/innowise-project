import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'fw-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'FBI-Wanted';
}
